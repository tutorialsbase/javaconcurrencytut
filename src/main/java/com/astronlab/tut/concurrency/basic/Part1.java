package com.astronlab.tut.concurrency.basic;

/*
-  Java included high-level concurrency APIs from JDK 5.0

 If a thread reads a memory location while another thread writes to it,
 - what value will the first thread end up reading?
 -- The old value?
 -- The value written by the second thread? 
 Or a value that is a mix between the two? 
 Or, if two threads are writing to the cognate memory location simultanously, 
 what value will be left when they are done? The value written by the first thread? 
 The value written by the second thread? Or a mix of the two values written?

 */
public class Part1 {

}
